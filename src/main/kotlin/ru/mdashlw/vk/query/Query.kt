package ru.mdashlw.vk.query

import com.fasterxml.jackson.databind.PropertyNamingStrategy
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

abstract class Query {
    open val data = mutableMapOf<String, Any>()

    class Parameter<T>(private val value: T? = null) : ReadWriteProperty<Query, T> {
        private fun adaptName(property: KProperty<*>): String =
            (PropertyNamingStrategy.SNAKE_CASE as PropertyNamingStrategy.SnakeCaseStrategy).translate(property.name)

        operator fun provideDelegate(thisRef: Query, property: KProperty<*>): Parameter<T> {
            value?.let {
                val name = adaptName(property)

                thisRef.data[name] = it as Any
            }

            return this
        }

        override fun getValue(thisRef: Query, property: KProperty<*>): T =
            throw UnsupportedOperationException()

        override fun setValue(thisRef: Query, property: KProperty<*>, value: T) {
            if (value == null) {
                return
            }

            val name = adaptName(property)

            thisRef.data[name] = value as Any
        }
    }
}
