package ru.mdashlw.vk.entity.base

/*
    There is an interesting situation about City entity.
    VK schema calls it a `base_object`,
    there is no City object in their API.
    But I decided to make one,
    because representing something as an Object,
    instead of a specific class for this is
    a stupid idea.
 */
class City(
    id: Int,
    title: String
) : Object(
    id,
    title
)
