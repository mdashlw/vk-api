package ru.mdashlw.vk.entity.base

class LikesInfo(
    val canLike: Boolean,
    val canPublish: Boolean?,
    val count: Int,
    val userLikes: Int
)
