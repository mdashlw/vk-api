package ru.mdashlw.vk.entity.base

class Geo(
    val coordinates: Coordinates?,
    val place: Place?,
    val showmap: Int?,
    val type: String?
) {
    class Coordinates(
        val latitude: Int,
        val longitude: Int
    )
}
