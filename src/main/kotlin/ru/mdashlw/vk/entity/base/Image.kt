package ru.mdashlw.vk.entity.base

class Image(
    val height: Int,
    val url: String,
    val width: Int
)
