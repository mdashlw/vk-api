package ru.mdashlw.vk.entity.base

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
enum class Sex {
    UNKNOWN,
    FEMALE,
    MALE;
}
