package ru.mdashlw.vk.entity.base

class Place(
    val address: String?,
    val checkins: Int?,
    val city: String?,
    val country: String?,
    val created: Int?,
    val icon: String?,
    val id: Int?,
    val latitude: Int?,
    val longitude: Int?,
    val title: String?,
    val type: String?
)
