package ru.mdashlw.vk.entity.users

open class UserMin(
    val deactivated: String?,
    val firstName: String,
    val hidden: Int?,
    val id: Int,
    val lastName: String
)
