package ru.mdashlw.vk.entity.wall

enum class PostType {
    POST,
    COPY,
    REPLY,
    POSTPONE,
    SUGGEST;
}
