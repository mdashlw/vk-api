package ru.mdashlw.vk.entity.wall

data class PostSource(
    val data: String?,
    val platform: String?,
    val type: Type?,
    val url: String?
) {
    enum class Type {
        VK,
        WIDGET,
        API,
        RSS,
        SMS;
    }
}
