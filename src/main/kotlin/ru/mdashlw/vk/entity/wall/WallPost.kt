package ru.mdashlw.vk.entity.wall

import ru.mdashlw.vk.entity.base.Geo
import ru.mdashlw.vk.entity.base.LikesInfo
import ru.mdashlw.vk.entity.base.RepostsInfo

data class WallPost(
    val accessKey: String?,
    val attachments: List<Attachment>?,
    val date: Int?,
    val edited: Int?,
    val fromId: Int?,
    val geo: Geo?,
    val id: Int?,
    val isArchived: Boolean?,
    val isFavorite: Boolean?,
    val likes: LikesInfo?,
    val ownerId: Int?,
    val postSource: PostSource?,
    val postType: PostType?,
    val reposts: RepostsInfo?,
    val signerId: Int?,
    val text: String?,
    val views: Views?
) {
    data class Attachment(
        val accessKey: String?,
        val album: Any?, // TODO
        val app: Any?, // TODO
        val audio: Any?, // TODO
        val doc: Any?, // TODO
        val event: Any?, // TODO
        val graffiti: Any?, // TODO
        val link: Any?, // TODO
        val market: Any?, // TODO
        val marketAlbum: Any?, // TODO
        val note: Any?, // TODO
        val page: Any?, // TODO
        val photo: Any?, // TODO
        val photosList: List<String>?,
        val poll: Any?, // TODO
        val postedPhoto: Any?, // TODO
        val type: Type,
        val video: Any? // TODO
    ) {
        enum class Type {
            PHOTO,
            POSTED_PHOTO,
            AUDIO,
            VIDEO,
            DOC,
            LINK,
            GRAFFITI,
            NOTE,
            APP,
            POLL,
            PAGE,
            ALBUM,
            PHOTOS_LIST,
            MARKET_MARKET_ALBUM,
            MARKET,
            EVENT;
        }
    }
}
