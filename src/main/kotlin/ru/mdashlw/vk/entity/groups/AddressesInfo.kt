package ru.mdashlw.vk.entity.groups

class AddressesInfo(
    val isEnabled: Boolean,
    val mainAddressId: Int?
)
