package ru.mdashlw.vk.entity.groups

class OnlineStatus(
    val minutes: Int?,
    val status: Type
) {
    enum class Type {
        NONE,
        ONLINE,
        ANSWER_MARK;
    }
}
