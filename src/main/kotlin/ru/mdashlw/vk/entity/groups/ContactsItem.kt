package ru.mdashlw.vk.entity.groups

class ContactsItem(
    val desc: String?,
    val email: String?,
    val phone: String?,
    val userId: Int?
)
