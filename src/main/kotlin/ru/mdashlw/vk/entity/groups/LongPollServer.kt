package ru.mdashlw.vk.entity.groups

class LongPollServer(
    val key: String,
    val server: String,
    val ts: String
)
