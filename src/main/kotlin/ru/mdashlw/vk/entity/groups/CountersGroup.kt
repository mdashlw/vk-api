package ru.mdashlw.vk.entity.groups

class CountersGroup(
    val addresses: Int?,
    val albums: Int?,
    val audios: Int?,
    val docs: Int?,
    val market: Int?,
    val photos: Int?,
    val topics: Int?,
    val videos: Int?
)
