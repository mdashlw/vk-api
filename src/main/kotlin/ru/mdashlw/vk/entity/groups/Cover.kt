package ru.mdashlw.vk.entity.groups

import ru.mdashlw.vk.entity.base.Image

class Cover(
    val enabled: Boolean,
    val images: List<Image>?
)
