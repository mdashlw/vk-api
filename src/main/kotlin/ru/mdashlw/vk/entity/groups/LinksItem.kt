package ru.mdashlw.vk.entity.groups

import com.fasterxml.jackson.annotation.JsonProperty

class LinksItem(
    val desc: String?,
    val editTitle: Boolean?,
    val id: Int?,
    val name: String?,
    @JsonProperty("photo_100") val photo100: String?,
    @JsonProperty("photo_50") val photo50: String?,
    val url: String?
)
