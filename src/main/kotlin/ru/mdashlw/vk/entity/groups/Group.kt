package ru.mdashlw.vk.entity.groups

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import ru.mdashlw.vk.entity.base.City
import ru.mdashlw.vk.entity.base.Country

open class Group(
    val adminLevel: AdminLevel?,
    val deactivated: String?,
    val finishDate: Int?,
    val id: Int?,
    val isAdmin: Boolean?,
    val isAdvertiser: Boolean?,
    val isClosed: IsClosed?,
    val isMember: Boolean?,
    val name: String?,
    @JsonProperty("photo_100") val photo100: String?,
    @JsonProperty("photo_200") val photo200: String?,
    @JsonProperty("photo_50") val photo50: String?,
    val screenName: String?,
    val startDate: Int?,
    val type: Type?
) {
    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    enum class AdminLevel {
        DEFAULT, // This does not exist in API, but it's a thing here for proper deserialization
        MODERATOR,
        EDITOR,
        ADMINISTRATOR;
    }

    @JsonFormat(shape = JsonFormat.Shape.NUMBER)
    enum class IsClosed {
        OPEN,
        CLOSED,
        PRIVATE;
    }

    enum class Type {
        GROUP,
        PAGE,
        EVENT;
    }

    class BanInfo(
        val comment: String?,
        val endDate: Int?
    )

    class Full(
        adminLevel: AdminLevel?,
        deactivated: String?,
        finishDate: Int?,
        id: Int?,
        isAdmin: Boolean?,
        isAdvertiser: Boolean?,
        isClosed: IsClosed?,
        isMember: Boolean?,
        name: String?,
        photo100: String?,
        photo200: String?,
        photo50: String?,
        screenName: String?,
        startDate: Int?,
        type: Type?,
        val market: MarketInfo?,
        val memberStatus: MemberStatus?,
        val isFavorite: Boolean?,
        val isSubscribed: Boolean?,
        val city: City?,
        val country: Country?,
        val verified: Boolean?,
        val description: String?,
        val wikiPage: String?,
        val membersCount: Int?,
        val counters: CountersGroup?,
        val cover: Cover?,
        val canPost: Boolean?,
        val canSeeAllPosts: Boolean?,
        val activity: String?,
        val fixedPost: Int?,
        val canCreateTopic: Boolean?,
        val canUploadVideo: Boolean?,
        val hasPhoto: Boolean?,
        val status: String?,
        val mainAlbumId: Int?,
        val links: List<LinksItem>?,
        val contacts: List<ContactsItem>?,
        val site: String?,
        val mainSection: MainSection?,
        val trending: Boolean?,
        val canMessage: Boolean?,
        val isMessagesBlocked: Boolean?,
        val canSendNotify: Boolean?,
        val onlineStatus: OnlineStatus?,
        val ageLimits: AgeLimits?,
        val banInfo: BanInfo?,
        val addresses: AddressesInfo?,
        val isSubscribedPodcasts: Boolean?,
        val canSubscribePodcasts: Boolean?,
        val canSubscribePosts: Boolean?
    ) : Group(
        adminLevel,
        deactivated,
        finishDate,
        id,
        isAdmin,
        isAdvertiser,
        isClosed,
        isMember,
        name,
        photo100,
        photo200,
        photo50,
        screenName,
        startDate,
        type
    ) {
        enum class MemberStatus {
            @JsonProperty("not a member")
            NOT_A_MEMBER,
            MEMBER,
            @JsonProperty("not sure")
            NOT_SURE,
            DECLINED,
            @JsonProperty("has sent a request")
            HAS_SENT_A_REQUEST,
            INVITED;
        }

        enum class MainSection {
            ABSENT,
            PHOTOS,
            TOPICS,
            AUDIO,
            VIDEO,
            MARKET;
        }

        enum class AgeLimits {
            NO,
            @JsonProperty("over 16")
            OVER_16,
            @JsonProperty("over 18")
            OVER_18;
        }
    }
}
