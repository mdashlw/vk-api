package ru.mdashlw.vk.entity.groups

import ru.mdashlw.vk.entity.market.Currency

class MarketInfo(
    val contactId: Int?,
    val currency: Currency?,
    val currencyText: String?,
    val enabled: Boolean?,
    val mainAlbumId: Int?,
    val priceMax: Int?,
    val priceMin: Int?
)
