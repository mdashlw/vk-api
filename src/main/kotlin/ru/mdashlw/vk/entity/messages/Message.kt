package ru.mdashlw.vk.entity.messages

import com.fasterxml.jackson.annotation.JsonProperty
import ru.mdashlw.vk.entity.base.Geo

class Message(
    val action: Action?,
    val adminAuthorId: Int?,
    val attachments: List<Attachment>?,
    val conversationMessageId: Int?,
    val date: Int,
    val deleted: Boolean?,
    val fromId: Int,
    val fwdMessages: List<ForeignMessage>?,
    val geo: Geo?,
    val id: Int,
    val important: Boolean?,
    val isHidden: Boolean?,
    val keyboard: Keyboard?,
    val membersCount: Int?,
    val out: Boolean,
    val payload: String?,
    val peerId: Int,
    val randomId: Int?,
    val replyMessage: ForeignMessage?,
    val text: String,
    val updateTime: Int?,
    val ref: String?,
    val refSource: String?
) {
    class Action(
        val conversationMessageId: Int?,
        val email: String?,
        val memberId: Int?,
        val message: String?,
        val photo: Photo?,
        val text: String?,
        val type: Status
    ) {
        class Photo(
            @JsonProperty("photo_100") val photo100: String,
            @JsonProperty("photo_200") val photo200: String,
            @JsonProperty("photo_50") val photo50: String
        )

        enum class Status {
            CHAT_PHOTO_UPDATE,
            CHAT_PHOTO_REMOVE,
            CHAT_CREATE,
            CHAT_TITLE_UPDATE,
            CHAT_INVITE_USER,
            CHAT_KICK_USER,
            CHAT_PIN_MESSAGE,
            CHAT_UNPIN_MESSAGE,
            CHAT_INVITE_USER_BY_LINK;
        }
    }

    class Attachment(
        val audio: Any?, // TODO
        val audioMessage: Any?, // TODO
        val doc: Any?, // TODO
        val gift: Any?, // TODO
        val graffiti: Any?, // TODO
        val link: Any?, // TODO
        val market: Any?, // TODO
        val marketMarketAlbum: Any?, // TODO
        val photo: Any?, // TODO
        val sticker: Any?, // TODO
        val type: Type,
        val video: Any?, // TODO
        val wall: Any?, // TODO
        val wallReply: Any? // TODO
    ) {
        enum class Type {
            PHOTO,
            AUDIO,
            VIDEO,
            DOC,
            LINK,
            MARKET_ALBUM,
            GIFT,
            STICKER,
            WALL,
            WALL_REPLY,
            GRAFFITI,
            AUDIO_MESSAGE;
        }
    }
}
