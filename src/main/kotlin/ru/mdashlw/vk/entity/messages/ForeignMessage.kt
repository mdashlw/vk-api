package ru.mdashlw.vk.entity.messages

import ru.mdashlw.vk.entity.base.Geo

class ForeignMessage(
    val attachments: List<Message.Attachment>?,
    val conversationMessageId: Int?,
    val date: Int,
    val fromId: Int,
    val fwdMessages: List<ForeignMessage>?,
    val geo: Geo?,
    val id: Int?,
    val peerId: Int?,
    val replyMessage: ForeignMessage?,
    val text: String,
    val updateTime: Int?
)
