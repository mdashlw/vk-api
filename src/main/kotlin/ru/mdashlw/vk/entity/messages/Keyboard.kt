package ru.mdashlw.vk.entity.messages

class Keyboard(
    val authorId: Int?,
    val buttons: List<List<Button>>,
    val oneTime: Boolean
) {
    class Button(
        val action: Action,
        val color: Color?
    ) {
        class Action(
            val appId: Int?,
            val hash: String?,
            val label: String?,
            val ownerId: Int?,
            val payload: String?,
            val type: Type
        ) {
            enum class Type {
                TEXT,
                START;
            }
        }

        enum class Color {
            DEFAULT,
            POSITIVE,
            NEGATIVE,
            PRIMARY;
        }
    }
}
