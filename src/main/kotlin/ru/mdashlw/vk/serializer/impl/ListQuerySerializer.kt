package ru.mdashlw.vk.serializer.impl

import ru.mdashlw.vk.serializer.QuerySerializer
import ru.mdashlw.vk.util.serializeToQuery

object ListQuerySerializer : QuerySerializer<List<*>>() {
    override fun serialize(value: List<*>): String =
        value.filterNotNull().joinToString(",", transform = Any::serializeToQuery)
}
