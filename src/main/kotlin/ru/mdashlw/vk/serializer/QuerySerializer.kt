package ru.mdashlw.vk.serializer

import ru.mdashlw.vk.VkClient

abstract class QuerySerializer<T : Any> {
    @Suppress("UNCHECKED_CAST")
    fun register(clazz: Class<T>) {
        VkClient.querySerializers[clazz] = this as QuerySerializer<Any>
    }

    abstract fun serialize(value: T): String
}
