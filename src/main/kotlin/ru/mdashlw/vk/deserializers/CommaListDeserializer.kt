package ru.mdashlw.vk.deserializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

object CommaListDeserializer : JsonDeserializer<List<*>>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): List<*> =
        p.text.split(",")
}
