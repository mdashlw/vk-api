package ru.mdashlw.vk.request

import ru.mdashlw.vk.VkClient
import ru.mdashlw.vk.query.Query
import ru.mdashlw.vk.response.Response
import ru.mdashlw.vk.util.joinToString
import ru.mdashlw.vk.util.newCall
import ru.mdashlw.vk.util.readValue
import ru.mdashlw.vk.util.serializeToQuery
import kotlin.collections.isNullOrEmpty
import kotlin.reflect.KClass

abstract class Request<R : Response<T>, T, Q : Query>(
    private val responseClass: KClass<R>,
    private val baseUrl: String
) {
    private fun buildUrl(query: Query): String =
        baseUrl +
                query.data
                    .takeUnless(Map<String, Any>::isNullOrEmpty)
                    ?.joinToString("&", "?") { key, value ->
                        "$key=${value.serializeToQuery()}"
                    }
                    .orEmpty()

    // TODO Error handling
    fun execute(query: Query): T {
        val url = buildUrl(query)
        val response = VkClient.okHttpClient.newCall(url)

        val value = VkClient.jackson.readValue(response, responseClass)

        return value.response
    }
}
