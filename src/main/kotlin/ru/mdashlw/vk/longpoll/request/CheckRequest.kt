package ru.mdashlw.vk.longpoll.request

import ru.mdashlw.vk.longpoll.entity.CheckEntity
import ru.mdashlw.vk.longpoll.query.CheckQuery
import ru.mdashlw.vk.longpoll.response.CheckResponse
import ru.mdashlw.vk.request.Request

class CheckRequest(server: String) :
    Request<CheckResponse, CheckEntity, CheckQuery>(
        CheckResponse::class,
        server
    )
