package ru.mdashlw.vk.longpoll.callback

import ru.mdashlw.vk.entity.messages.Message
import ru.mdashlw.vk.entity.wall.WallPost

interface LongPollCallback {
    fun wallPostNew(post: WallPost, groupId: Int) = Unit

    fun messageNew(message: Message, groupId: Int) = Unit
}
