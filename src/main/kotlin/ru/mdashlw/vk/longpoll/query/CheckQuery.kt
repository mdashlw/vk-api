package ru.mdashlw.vk.longpoll.query

import ru.mdashlw.vk.query.Query

class CheckQuery : Query() {
    val act by Parameter("a_check")

    var key by Parameter<String>()
    var ts by Parameter<String>()
    var wait by Parameter(25)
}
