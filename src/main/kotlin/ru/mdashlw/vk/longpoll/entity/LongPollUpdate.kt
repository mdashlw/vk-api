package ru.mdashlw.vk.longpoll.entity

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import ru.mdashlw.vk.longpoll.entity.updates.messages.MessageNew
import ru.mdashlw.vk.longpoll.entity.updates.wall.WallPostNew

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    JsonSubTypes.Type(value = WallPostNew::class, name = "wall_post_new"),
    JsonSubTypes.Type(value = MessageNew::class, name = "message_new")
)
abstract class LongPollUpdate<out T : Any>(
    val type: String,
    val `object`: T,
    val groupId: Int
)
