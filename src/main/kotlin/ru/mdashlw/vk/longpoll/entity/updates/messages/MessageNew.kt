package ru.mdashlw.vk.longpoll.entity.updates.messages

import ru.mdashlw.vk.entity.messages.Message
import ru.mdashlw.vk.longpoll.entity.LongPollUpdate

class MessageNew(`object`: Message, groupId: Int) : LongPollUpdate<Message>("message_new", `object`, groupId)
