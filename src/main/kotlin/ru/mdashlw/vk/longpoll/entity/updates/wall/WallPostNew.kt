package ru.mdashlw.vk.longpoll.entity.updates.wall

import ru.mdashlw.vk.entity.wall.WallPost
import ru.mdashlw.vk.longpoll.entity.LongPollUpdate

class WallPostNew(`object`: WallPost, groupId: Int) : LongPollUpdate<WallPost>("wall_post_new", `object`, groupId)
