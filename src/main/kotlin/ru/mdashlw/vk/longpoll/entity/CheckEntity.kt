package ru.mdashlw.vk.longpoll.entity

data class CheckEntity(
    val ts: String,
    val updates: List<LongPollUpdate<Any>>
)
