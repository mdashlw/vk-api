package ru.mdashlw.vk.longpoll

import ru.mdashlw.vk.entity.groups.LongPollServer
import ru.mdashlw.vk.longpoll.callback.LongPollCallback
import ru.mdashlw.vk.longpoll.entity.updates.messages.MessageNew
import ru.mdashlw.vk.longpoll.entity.updates.wall.WallPostNew
import ru.mdashlw.vk.longpoll.query.CheckQuery
import ru.mdashlw.vk.longpoll.request.CheckRequest

class LongPoll(
    private val callback: LongPollCallback,
    private val server: String,
    private val key: String,
    private var ts: String
) {
    constructor(callback: LongPollCallback, server: LongPollServer) : this(
        callback,
        server.server,
        server.key,
        server.ts
    )

    var isRunning: Boolean = true

    fun start() {
        while (isRunning) {
            check()
        }
    }

    fun check() {
        val query = CheckQuery().apply {
            key = this@LongPoll.key
            ts = this@LongPoll.ts
        }
        val request = CheckRequest(server).execute(query)

        ts = request.ts

        request.updates.forEach {
            when (it) {
                is WallPostNew -> callback.wallPostNew(it.`object`, it.groupId)
                is MessageNew -> callback.messageNew(it.`object`, it.groupId)
            }
        }
    }

    fun stop() {
        isRunning = false
    }
}
