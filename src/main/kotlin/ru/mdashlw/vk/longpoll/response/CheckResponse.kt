package ru.mdashlw.vk.longpoll.response

import ru.mdashlw.vk.longpoll.entity.CheckEntity
import ru.mdashlw.vk.longpoll.entity.LongPollUpdate
import ru.mdashlw.vk.response.Response

class CheckResponse(ts: String, updates: List<LongPollUpdate<Any>>) : Response<CheckEntity>(CheckEntity(ts, updates))
