package ru.mdashlw.vk.response

abstract class Response<T>(
    val response: T
)
