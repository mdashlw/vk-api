package ru.mdashlw.vk.api.category.impl.messages

import ru.mdashlw.vk.api.category.Category
import ru.mdashlw.vk.api.category.impl.messages.query.SendQuery
import ru.mdashlw.vk.api.category.impl.messages.request.SendRequest

object Messages : Category() {
    fun send(block: SendQuery.() -> Unit) =
        execute(SendRequest, SendQuery(), block)
}
