package ru.mdashlw.vk.api.category.impl.users.query

import ru.mdashlw.vk.api.query.ApiQuery
import ru.mdashlw.vk.entity.users.Fields

class GetQuery : ApiQuery() {
    var userIds by Parameter<List<String>>()
    var fields by Parameter<List<Fields>>()
    var nameCase by Parameter<String>() // TODO enum
}
