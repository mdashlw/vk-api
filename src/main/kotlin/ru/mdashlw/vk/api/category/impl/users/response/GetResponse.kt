package ru.mdashlw.vk.api.category.impl.users.response

import ru.mdashlw.vk.entity.users.User
import ru.mdashlw.vk.response.Response

class GetResponse(response: List<User.XtrCounters>) : Response<List<User.XtrCounters>>(response)
