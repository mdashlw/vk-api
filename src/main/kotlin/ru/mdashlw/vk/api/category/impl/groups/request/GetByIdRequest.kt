package ru.mdashlw.vk.api.category.impl.groups.request

import ru.mdashlw.vk.api.category.impl.groups.query.GetByIdQuery
import ru.mdashlw.vk.api.category.impl.groups.response.GetByIdResponse
import ru.mdashlw.vk.api.request.ApiRequest
import ru.mdashlw.vk.entity.groups.Group

object GetByIdRequest :
    ApiRequest<GetByIdResponse, List<Group.Full>, GetByIdQuery>(
        GetByIdResponse::class,
        "groups.getById"
    )
