package ru.mdashlw.vk.api.category.impl.messages.query

import ru.mdashlw.vk.api.query.ApiQuery
import ru.mdashlw.vk.entity.messages.Keyboard
import kotlin.random.Random

class SendQuery : ApiQuery() {
    var userId by Parameter<Int>()
    var randomId by Parameter(Random.nextInt())
    var peerId by Parameter<Int>()
    var domain by Parameter<String>()
    var chatId by Parameter<Int>()
    var userIds by Parameter<List<Int>>()
    var message by Parameter<String>()
    var lat by Parameter<Int>()
    var long by Parameter<Int>()
    var attachment by Parameter<String>() // TODO
    var replyTo by Parameter<Int>()
    var forwardMessages by Parameter<List<String>>()
    var stickerId by Parameter<Int>()
    var groupId by Parameter<Int>()
    var keyboard by Parameter<Keyboard>() // TODO Query serializer
    var payload by Parameter<String>()
    var dontParseLinks by Parameter<Boolean>()
    var disableMentions by Parameter<Boolean>()
}
