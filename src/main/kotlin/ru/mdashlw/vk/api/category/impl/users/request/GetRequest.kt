package ru.mdashlw.vk.api.category.impl.users.request

import ru.mdashlw.vk.api.category.impl.users.query.GetQuery
import ru.mdashlw.vk.api.category.impl.users.response.GetResponse
import ru.mdashlw.vk.api.request.ApiRequest
import ru.mdashlw.vk.entity.users.User

object GetRequest :
    ApiRequest<GetResponse, List<User.XtrCounters>, GetQuery>(
        GetResponse::class,
        "users.get"
    )
