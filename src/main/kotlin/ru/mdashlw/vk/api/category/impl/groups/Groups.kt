package ru.mdashlw.vk.api.category.impl.groups

import ru.mdashlw.vk.api.category.Category
import ru.mdashlw.vk.api.category.impl.groups.query.GetByIdQuery
import ru.mdashlw.vk.api.category.impl.groups.query.GetLongPollServerQuery
import ru.mdashlw.vk.api.category.impl.groups.request.GetByIdRequest
import ru.mdashlw.vk.api.category.impl.groups.request.GetLongPollServerRequest

object Groups : Category() {
    fun getLongPollServer(block: GetLongPollServerQuery.() -> Unit) =
        execute(GetLongPollServerRequest, GetLongPollServerQuery(), block)

    fun getById(block: GetByIdQuery.() -> Unit) =
        execute(GetByIdRequest, GetByIdQuery(), block)
}
