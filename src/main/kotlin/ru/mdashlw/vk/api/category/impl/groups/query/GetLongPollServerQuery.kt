package ru.mdashlw.vk.api.category.impl.groups.query

import ru.mdashlw.vk.api.query.ApiQuery

class GetLongPollServerQuery : ApiQuery() {
    var groupId by Parameter<Int>()
}
