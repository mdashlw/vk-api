package ru.mdashlw.vk.api.category.impl.users

import ru.mdashlw.vk.api.category.Category
import ru.mdashlw.vk.api.category.impl.users.query.GetQuery
import ru.mdashlw.vk.api.category.impl.users.request.GetRequest

object Users : Category() {
    fun get(block: GetQuery.() -> Unit) =
        execute(GetRequest, GetQuery(), block)
}
