package ru.mdashlw.vk.api.category.impl.groups.request

import ru.mdashlw.vk.api.category.impl.groups.query.GetLongPollServerQuery
import ru.mdashlw.vk.api.category.impl.groups.response.GetLongPollServerResponse
import ru.mdashlw.vk.api.request.ApiRequest
import ru.mdashlw.vk.entity.groups.LongPollServer

object GetLongPollServerRequest :
    ApiRequest<GetLongPollServerResponse, LongPollServer, GetLongPollServerQuery>(
        GetLongPollServerResponse::class,
        "groups.getLongPollServer"
    )
