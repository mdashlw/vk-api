package ru.mdashlw.vk.api.category.impl.messages.response

import ru.mdashlw.vk.response.Response

class SendResponse(response: Int) : Response<Int>(response)
