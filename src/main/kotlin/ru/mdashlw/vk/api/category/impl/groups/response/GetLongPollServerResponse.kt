package ru.mdashlw.vk.api.category.impl.groups.response

import ru.mdashlw.vk.entity.groups.LongPollServer
import ru.mdashlw.vk.response.Response

class GetLongPollServerResponse(response: LongPollServer) : Response<LongPollServer>(response)
