package ru.mdashlw.vk.api.category

import ru.mdashlw.vk.VkClient
import ru.mdashlw.vk.api.request.ApiRequest
import ru.mdashlw.vk.query.Query
import ru.mdashlw.vk.response.Response

abstract class Category {
    inline fun <Q : Query, R : Response<T>, T> execute(request: ApiRequest<R, T, Q>, query: Q, block: Q.() -> Unit): T =
        VkClient.execute(request, query, block)
}
