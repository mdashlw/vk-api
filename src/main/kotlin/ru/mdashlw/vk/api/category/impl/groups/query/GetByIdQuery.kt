package ru.mdashlw.vk.api.category.impl.groups.query

import ru.mdashlw.vk.api.query.ApiQuery
import ru.mdashlw.vk.entity.groups.Fields

class GetByIdQuery : ApiQuery() {
    var groupIds by Parameter<List<String>>()
    var groupId by Parameter<String>()
    var fields by Parameter<List<Fields>>()
}
