package ru.mdashlw.vk.api.category.impl.groups.response

import ru.mdashlw.vk.entity.groups.Group
import ru.mdashlw.vk.response.Response

class GetByIdResponse(response: List<Group.Full>) : Response<List<Group.Full>>(response)
