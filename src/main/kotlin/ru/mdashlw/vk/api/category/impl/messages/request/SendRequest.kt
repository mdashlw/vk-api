package ru.mdashlw.vk.api.category.impl.messages.request

import ru.mdashlw.vk.api.category.impl.messages.query.SendQuery
import ru.mdashlw.vk.api.category.impl.messages.response.SendResponse
import ru.mdashlw.vk.api.request.ApiRequest

object SendRequest :
    ApiRequest<SendResponse, Int, SendQuery>(
        SendResponse::class,
        "messages.send"
    )
