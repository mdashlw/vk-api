package ru.mdashlw.vk.api.query

import ru.mdashlw.vk.VkClient
import ru.mdashlw.vk.query.Query

abstract class ApiQuery : Query() {
    var accessToken by Parameter(VkClient.accessToken)
    var v by Parameter(VkClient.API_VERSION)
}
