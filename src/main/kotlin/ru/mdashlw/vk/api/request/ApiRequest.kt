package ru.mdashlw.vk.api.request

import ru.mdashlw.vk.query.Query
import ru.mdashlw.vk.request.Request
import ru.mdashlw.vk.response.Response
import kotlin.reflect.KClass

abstract class ApiRequest<R : Response<T>, T, Q : Query>(responseClass: KClass<R>, method: String) :
    Request<R, T, Q>(responseClass, "https://api.vk.com/method/$method")
