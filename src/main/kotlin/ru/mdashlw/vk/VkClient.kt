package ru.mdashlw.vk

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.OkHttpClient
import ru.mdashlw.vk.deserializers.BooleanDeserializer
import ru.mdashlw.vk.query.Query
import ru.mdashlw.vk.request.Request
import ru.mdashlw.vk.response.Response
import ru.mdashlw.vk.serializer.QuerySerializer
import ru.mdashlw.vk.serializer.impl.ListQuerySerializer
import ru.mdashlw.vk.util.addDeserializer
import java.util.concurrent.TimeUnit

object VkClient {
    const val API_VERSION = "5.95"

    internal val okHttpClient: OkHttpClient = OkHttpClient.Builder()
        .apply {
            connectTimeout(30, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
        }
        .build()

    internal val jackson = jacksonObjectMapper()
        .apply {
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS, true)
            propertyNamingStrategy = PropertyNamingStrategy.SNAKE_CASE

            registerModule(
                SimpleModule().apply {
                    addDeserializer(BooleanDeserializer)
                }
            )
        }

    val querySerializers = mutableMapOf<Class<out Any>, QuerySerializer<Any>>()

    lateinit var accessToken: String
        private set

    fun setup(accessToken: String) {
        VkClient.accessToken = accessToken
        registerQuerySerializers()
    }

    private fun registerQuerySerializers() {
        ListQuerySerializer.register(List::class.java)
    }

    inline fun <Q : Query, R : Response<T>, T> execute(request: Request<R, T, Q>, query: Q, block: Q.() -> Unit): T =
        request.execute(query.apply(block))
}
