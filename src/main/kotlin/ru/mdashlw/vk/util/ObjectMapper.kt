@file:Suppress("NOTHING_TO_INLINE")

package ru.mdashlw.vk.util

import com.fasterxml.jackson.databind.ObjectMapper
import kotlin.reflect.KClass

internal inline fun <T : Any> ObjectMapper.readValue(content: String, valueType: KClass<T>): T =
    readValue(content, valueType.java)
