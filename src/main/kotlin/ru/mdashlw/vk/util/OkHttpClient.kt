package ru.mdashlw.vk.util

import okhttp3.OkHttpClient
import okhttp3.Request

internal fun OkHttpClient.newCall(url: String): String {
    val request = Request.Builder()
        .apply {
            url(url)
        }
        .build()

    val response = newCall(request).execute()

    return response.body()!!.string()
}
