package ru.mdashlw.vk.util

import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule

internal inline fun <reified T : Any> SimpleModule.addDeserializer(deser: JsonDeserializer<T>) {
    addDeserializer(T::class.java, deser)
}
