package ru.mdashlw.vk.util

import ru.mdashlw.vk.VkClient

internal fun Any.serializeToQuery(): String =
    VkClient.querySerializers[javaClass]
        ?.serialize(this)
        ?: toString()
