package ru.mdashlw.vk.util

// TODO Move to ru.mdashlw.util:common-util
internal fun <K, V> Map<K, V>.joinToString(
    separator: CharSequence = ", ",
    prefix: CharSequence = "",
    postfix: CharSequence = "",
    limit: Int = -1,
    truncated: CharSequence = "...",
    transform: ((K, V) -> CharSequence)? = null
) =
    joinTo(StringBuilder(), separator, prefix, postfix, limit, truncated, transform).toString()

// TODO Move to ru.mdashlw.util:common-util
internal fun <K, V, A : Appendable> Map<K, V>.joinTo(
    buffer: A,
    separator: CharSequence = ", ",
    prefix: CharSequence = "",
    postfix: CharSequence = "",
    limit: Int = -1,
    truncated: CharSequence = "...",
    transform: ((K, V) -> CharSequence)? = null
): A {
    buffer.append(prefix)

    var count = 0

    forEach { key, value ->
        if (++count > 1) {
            buffer.append(separator)
        }

        if (limit < 0 || count <= limit) {
            buffer.appendElement(key, value, transform)
        } else {
            return@forEach
        }
    }

    if (limit in 0 until count) {
        buffer.append(truncated)
    }

    buffer.append(postfix)

    return buffer
}
