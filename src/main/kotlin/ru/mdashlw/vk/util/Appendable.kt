package ru.mdashlw.vk.util

// TODO Move to ru.mdashlw.util:common-util
internal fun <K, V> Appendable.appendElement(key: K, value: V, transform: ((K, V) -> CharSequence)?) {
    if (transform != null) {
        append(transform(key, value))
    } else {
        append("$key=$value")
    }
}
